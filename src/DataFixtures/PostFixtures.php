<?php

namespace App\DataFixtures;

use App\Entity\Articles;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PostFixtures extends Fixture
{
    
    public function load(ObjectManager $manager)
    {
        $article = new Articles();
        $article->setTitle('5 sustainable sneakers to shop winter 2019')
                ->setAuthor('jld Yacynthe')
                ->setImgPath('images/rombaut.jpg')
                ->setContent('Sneakers vegan, ne veux pas toujours dire stan smith remasteurisées, il existe une palette beaucoup plus étendue de sneakers originales à shopper ! découvrez quelques un de nos modèles préférés :)Des sneakers 100% vegan, une identité marqué, Rombaut à l’instar de nombreuses marques repense la sneakers à travers un design à l’identité marquée. Les formes, et les couleurs offrent un renouveau essentiel pour séduire un public éco-conscient en mal de nouveauté.');
        $manager->persist($article);
    
        $article = new Articles();
        $article->setTitle('4 Upcycling designers')
                ->setAuthor('Melis Lys')
                ->setImgPath('images/ma2.jpg')
                ->setContent('Given the state of the planet, many of us are trying to buy less and less new stuff. After all, buying new only creates demand for a whole supply chain of destruction, from planting heavily sprayed monoculture crops like cotton to pouring toxic dyes into rivers.  Yet there is a conundrum: if we buy less, that’s great for the planet but bad for not only the economy overall, but the millions of people who rely on the manufacturing industries for their livelihoods. So how can we maintain economic growth without being destructive? There are ways – and buying upcycled fashion is one of them.In case you’re not familiar with the upcycling fashion concept, it involves using pre-existing clothing, accessories or other items and restructuring them into new garments. Think of old sweaters being unravelled and the yarn being refashioned into new ones, or scraps of fabric waste from car interiors being used to make handbags. Deadstock, otherwise known as fabric that’s leftover from the fashion industry, can also be used to make new clothing.');
        $manager->persist($article);
        
        $article = new Articles();
        $article->setTitle('Sustainable statement blazer')
                ->setAuthor('flu Recent')
                ->setImgPath('images/hungeruk.png')
                ->setContent('Lorem ipsum dolor sit, amet consectetur adipisicing elit. Molestiae tenetur qui, aspernatur labore non exercitationem perferendis placeat quam quis ipsa nihil excepturi iusto voluptatum impedit omnis sapiente, natus ad error?Lorem ipsum dolor sit, amet consectetur adipisicing elit. Molestiae tenetur qui, aspernatur labore non exercitationem perferendis placeat quam quis ipsa nihil excepturi iusto voluptatum impedit omnis sapiente, natus ad error?');
        $manager->persist($article);

        $article = new Articles();
        $article->setTitle('Guide to decluttering your wardrobe')
                ->setAuthor('dek Clutter')
                ->setImgPath('https://images.unsplash.com/photo-1559324926-ad3e8bab9df1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80')
                ->setContent('A minimal approach to fashion, as opposed to a minimal aesthetic for fashion, is more about the attitude and thought process behind things than it is about your color palette (or lack thereof) or the specific number of items in your closet.Having a minimal approach to fashion means that you approach your closet with intentionality, not aiming for the largest (or smallest) number of things. Rather, your aim is a wardrobe that fits your lifestyle, and is filled with high-quality pieces you absolutely love that will, hopefully, last years.The goal isn’t getting your shoes or dresses down to single digits. The goal isn’t only having shades of black, grey, cream, and taupe. The goal isn’t to make you hate your minimalist closet. In fact, it’s the exact opposite.');

        $manager->persist($article);
        
        $manager->flush();
    }
}
