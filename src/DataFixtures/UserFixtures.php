<?php

namespace App\DataFixtures;

use App\Entity\BlogUser;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class UserFixtures extends Fixture
{
    private $encoder;
    
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    public function load(ObjectManager $manager)
    {
        $user = new BlogUser();

        // Hashing du mot de passe.
        $hashedPassword = $this->encoder->encodePassword($user, '1234');

        $user->setUsername("jld")
            ->setEmail("moi@moi.com")
            ->setPassword($hashedPassword)
            ->setRoles(["ROLE_USER"]);
            $manager->persist($user);

        $manager->flush();
    }
}
