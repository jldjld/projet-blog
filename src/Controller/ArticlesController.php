<?php

namespace App\Controller;

use App\Entity\Articles;
use App\Entity\Subscribers;
use App\Form\ArticleType;
use App\Repository\ArticlesRepository;
use App\Repository\SubscribersRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ArticlesController extends AbstractController
{
      /**
     * @Route ("/", name="index")
     */
    public function accueil(ArticlesRepository $articlesRepository){
        $all = $articlesRepository->findAll();
        return $this->render('index.html.twig',[
            'all' => $all
        ]);
    }
    public function getSubscribers(Request $request){
        $mail = $request->get("newsletter");

        if($mail){
            $subscribers = new Subscribers($mail);

            $subRepo = new SubscribersRepository();

            $subRepo->add($subscribers);
        }
    }
    /**
     * @Route ("/blogform", name="blogform")
     */
    public function getArticle(Request $request, ObjectManager $manager){
        
        $article = new Articles();

        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $article = $form->getData();
            // $repo->add($article);
            $manager->persist($article);
            $manager->flush();
            return $this->redirectToRoute("index");
        }
        return $this->render('blogform.html.twig', [
            'formArticle' => $form->createView()
           
    ]);
    }
    

        // $article = null;
        
        // $title = $request->get("title");
        // $author = $request->get("author");
        // //$postDate = $request->get("postDate");
        // $content = $request->get("content");
        // $imgPath = $request->get("imgPath");

        

        // if($title && $author && $content && $imgPath) {
        //     //$postDate = new \DateTime();
        //     $article = new Articles($title,$author,$content,$imgPath);
            
        //     $repo->add($article);
            
        // // }
        // $all = $repo->findAll($article);

        

}
