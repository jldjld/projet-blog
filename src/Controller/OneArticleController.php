<?php

namespace App\Controller;

use App\Entity\Articles;
use App\Repository\ArticlesRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class OneArticleController extends AbstractController
{
    /**
     * @Route("/{id}", name="article_id")
     */
    public function showArticleId(int $id, ArticlesRepository $articlesRepository){
        //$articlesRepository = new ArticlesRepository();
        $article = $articlesRepository->find($id);
        //dump($articles);
        //dump($id);
        return $this->render('post.html.twig', [
            'article' => $article
        ]);

    }

    /**
    * @Route("/del/{id}", name="delete")
    */
   public function deletePost(int $id, ArticlesRepository $repo, ObjectManager $manager)
   {
    $article = $repo->find($id);
    $manager->remove($article);
    
    $manager->flush();
    
      return $this->redirectToRoute('index');
   }
//    /**
//     * @Route("/edit/{id}", name="edit")
//     */
//     public function EditPost(int $id, ObjectManager $manager){
//         $manager = $this->getDoctrine()->getManager();
//         $article = $manager->getRepository(Articles::class)->find($id);

//     if (!$article) {
//         throw $this->createNotFoundException(
//             'There is no article here '.$id
//         );
//     }

//     $article->setTitle('New article title!');
//     $article->setAuthor('New author!');
//     $article->setImgpath('New img!');
//     $article->setContent('New content!');
//     $manager->flush();

//     return $this->redirectToRoute('edit.html.twig', [
//         'editArticle' => $article->getId()
//     ]);
//     }

}
