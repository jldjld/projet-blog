# PROJET BLOG V1

##### CONTEXTE /

Durant mon cursus de formation de développeur web et mobile à Simplon Lyon, il m'a été demandé de réaliser une plateforme de blogging,
sur laquelle on viendrais implémenter les fonctionnalités suivantes:

- Créer des articles
- Consulter la liste des articles
- Consulter un article spécifique
- supprimer un article

En utilisant les framework suivants:

- mysql
- PHP
- Symfony
- Bootstrap 4.3


##### IDÉE /

J'ai souhaité créer une plateforme de blog participatif pour mettre en avant la conssomation éthique dans le domaine du prêt-à-porter,
et favoriser la mise en commun des ressources, des idées pour les personnes qui souhaitent modifier leurs habitudes de conssomation.

##### FONCTIONNALITÉS MISE EN PLACE POUR CE PROJET /

Pour ce projet, il me paraissait intéréssant de pouvoir ajouter une fonctionnalité d'authentification.
Pour la route ajout d'un article, rende au minimum obligatoire la création d'un compte sur le site.

img index

###### USE CASE /

![5-Homepage-mobile](public/images/usecaseblog.png)


###### MAQUETTE /

<u>Version mobile /</u> 

Ce projet à été pensé en mobile first

![5-Homepage-mobile](public/images/wireframe.png)



<u>Version mobile /</u> page article

![9-article-mobile](public/images/9-article-mobile.png)

###### SCREENSHOTS /

<u>Version mobile /</u> 

![accueil](public/images/screenshot.png)


###### A mettre en place en V2 /

- fonctionnalité modifier un article
- option de recherche d'un article
- Mise en place de commentaires articles
